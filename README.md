keepstate
=========

Notify when command output changes

This simple script:
- executes a given command
- compares output (exit code, standard output & error) with
    previous saved command output
- sends a mail when output differs
- saves current output for further comparisons
